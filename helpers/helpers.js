import { Todo } from "../models/config";

const getAllTodos = async (req,res) => {
    try {
        const datas = await Todo.find()
        res.json(datas)
    } catch (error) {
        res.json(error)
    }
}

const postAnItem = async (req, res) => {
    try {
        const data = await Todo.create({name:req.query.name})
        res.json(data); 
    } catch (error) {
        res.json(error)
    }
}

const getATodo = async (req,res) => {
    try {
        const item = await Todo.findById(req.params.id)
        res.json(item)
    } catch (error) {
        res.json(error)
    }
}
const updateCheckState = async(req, res) => {
    try {
        const data = await Todo.findOne({_id:req.params.id})
        const checkState = !data.isChecked
        const newData = await Todo.findOneAndUpdate({_id:req.params.id},{isChecked:checkState},{new:true}) 
        res.json(newData)
    } catch (error) {
        res.json(error)
    }
}
const deleteATodo = async (req, res) => {
    try {
        const item = await Todo.findOneAndDelete({_id:req.query.id})
        res.json(`The item has deleted ${item.name}`)
    } catch (error) {
        res.json(error)
    }
}
export { getAllTodos, getATodo, postAnItem ,updateCheckState,deleteATodo}