import mongoose from "mongoose";


const TodoSchema = new mongoose.Schema({
    name:{
        type:String,
        required:"name cannot be blank"
    },
    isChecked:{
        type:Boolean,
        default:false
    },
    createdAt:{
        type:Date,
        default:Date.now
    }
})

const Todo = mongoose.model("Todo", TodoSchema);

export default Todo;