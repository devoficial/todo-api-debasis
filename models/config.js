import mongodb from "mongodb";
import mongoose from "mongoose";
mongoose.set("debug",true);
mongoose.Promise = Promise;
import Todo from "./Todo";
// Setting up the connection
const connection = mongoose.connect("mongodb://localhost/todoApi")

export { connection, Todo }
