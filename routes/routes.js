import express from "express";
const router = express.Router()
import { getAllTodos, getATodo , postAnItem, updateCheckState, deleteATodo} from "../helpers/helpers";

router.route("/")
.get(getAllTodos)
.post(postAnItem)
.delete(deleteATodo)

router.route("/:id")
.get(getATodo)
.put(updateCheckState)



export default router;