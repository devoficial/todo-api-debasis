import express from "express";
const App = express()
const PORT  = process.env.PORT || 3000;
import { connection } from "./models/config";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import todoRoutes from "./routes/routes";
import cors from "cors";

App.use(bodyParser.json());
App.use(bodyParser.urlencoded({extended:true}));
App.use(cors())

const db = mongoose.connection;
db.once("open" , () => console.log(`database is connected ${db}`))


App.get("/",(req, res) => res.json({"visit":"please use the /api/todos/ routes to get the datas"}))
App.use("/api/todos", todoRoutes)

App.listen(PORT , () => {
    console.log(`The app is listening at port ${PORT}`)
})